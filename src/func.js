const getSum = (str1, str2) => {
  if(typeof(str1) != 'string' || typeof(str2) != 'string') return false;
  if(str1 === '') for(let i = 0; i < str2.length; i++) str1 += '0';
  if(str2 === '') for(let i = 0; i < str1.length; i++) str2 += '0';
  let res = '';
  for (let i = 0; i < str1.length; i++) {
    let a = parseInt(str1[i]);
    let b = parseInt(str2[i]);
    if(isNaN(a)) return false;
    if(isNaN(b)) return false;
    res += (a+b).toString();
  }

  return res;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let res = {Post:0, comments:0};
  for (const post of listOfPosts) {
    if(post.author === authorName) res.Post++;
    if(typeof(post.comments) != 'undefined'){
      for (const comment of post.comments) {
        if(comment.author === authorName) res.comments++;
      }
    }
  }
  return 'Post:' + res.Post.toString() + ',comments:' + res.comments.toString();
};

const tickets=(people)=> {
  let money = 0;
  let cost = 25;
  for (const person of people) {
    if(person != cost){
      if(person - cost > money) return 'NO';
      else money += cost - person + cost;
    } else {
      money += cost;
    }
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
